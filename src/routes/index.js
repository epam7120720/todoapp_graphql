const express = require('express');
const router = express.Router();
const taskController = require('../controllers/tasks');
const isAuthenticated = require('../middlewares/isAuthenticated');

router.use(isAuthenticated);

router.post('/',taskController.addTask);

router.get('/', taskController.getTasks);

router.put('/done', taskController.makeDone);

router.get('/done', taskController.getDones);

router.put('/done/:taskTitle', taskController.editTask);

router.delete('/done', taskController.deleteTask)

module.exports=()=>{return router}