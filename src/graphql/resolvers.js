const db = require('../Models/db');
const tasks = db.tasks;


const resolvers = {
    Query:{
        getTodoTasks:async(_,args, context)=>{
            console.log("xxxxxxxxxxx",context);
            const user_id =context.user.user_id;
            try{
           const reTasks= await tasks.findAll({where:{user_id}});
           return reTasks;
            }catch(err){
                console.error(err);
            }
        },
        getDoneTasks:async(_,args, context)=>{
            const user_id = context.user.user_id;
            try{
           const reTasks= await tasks.findAll({where:{user_id, isDone:1}});
           return reTasks;
            }catch(err){
                console.error(err);
            }
        },
        getTask:async(_,args, context)=>{
            const{id}= args;
            try{
           const task=  await tasks.findByPk(id);
           return task;
            }catch(err){
                console.error(err);
            }
        }, 
    },
    Mutation:{
    createTask: async(_,args, context)=>{
       
        const user_id = context.user.user_id;;
        const{title, description}= args.task;
        try{
        const task= await tasks.create({user_id,title, description});
        return task;
        }catch(err){
            console.error(err);
        }
    },
    updateTask: async(_,args, context)=>{
        const {task} = args;
        const{id}=args;
        const user_id = context.user.user_id;;
        
        const existingTask = await tasks.findByPk(id);
        if (!existingTask) {
          throw new Error('Task not found');
        }

        if (existingTask.user_id !== user_id) {
          throw new Error('Not authorized to update this task');
        }

        try {
          await existingTask.update(task);
          return existingTask;
        } catch (error) {
          throw new Error('Error updating task');
        }
      },
    deleteTask: async(_,args, context)=>{
        const { id } = args;
        console.log('aasasssssssss',args);
        try {
          await tasks.destroy({where:{id}});
          return true; 
        } catch (error) {
          throw new Error('Error deleting task');
        }
      }
    }
}

module.exports= resolvers;

