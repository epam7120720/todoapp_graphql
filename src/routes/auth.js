const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth')
const authCheck = require('../middlewares/authCheck');

router.use(authCheck);

router.post('/register',authController.register);

router.post('/login' , authController.login);
    
module.exports=()=>{return router;}