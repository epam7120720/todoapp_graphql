const express = require("express");
const makeStoppable = require("stoppable")
const http = require("http");
const cookeiSession= require('cookie-session');
const bodyParser = require('body-parser');
const routes = require('./routes')
const authRoutes = require('./routes/auth')
const{graphqlHTTP} = require('express-graphql');
const schemaa = require('./graphql/schema');
const isAuthenticated = require('./middlewares/isAuthenticated')
const app = express();



require('./Models/db');

app.use(
  cookeiSession({
    name:'session',
    keys:['sdfax123']
  })
)

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(
  '/api/auth/',
  authRoutes());

app.use(
  '/api/tasks/',
  routes());

app.use('/graphql',isAuthenticated, graphqlHTTP((req, res, graphQLParams) => {
  return {
    schema:schemaa,
    // other options
    context: {
      user: req.user,
      // whatever else you want
    }
  }
} ))
const server = makeStoppable(http.createServer(app));

module.exports = () => {
  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    })
  };

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
}
