const { makeExecutableSchema } = require('graphql-tools');
const resolvers= require('./resolvers');
const typeDefs = `
type Task { 
  id: ID!
  title: String! 
  description: String!
  isDone: Boolean! 
  createdAt:String!
  updatedAt:String!
  user_id:ID!
}

type Query { 
  getTodoTasks: [Task]! 
  getDoneTasks: [Task]! 
  getTask(id: ID): Task 
}

input CreateTaskInput { 
  title: String! 
  description: String! 
}

input UpdateTaskInput { 
  title: String!
  description: String!  
}

type Mutation { 
  createTask(task: CreateTaskInput!): Task! 
  updateTask(id: ID!, task: UpdateTaskInput!): Task! 
  deleteTask(id: ID!): Boolean 
}
`;

const schema = makeExecutableSchema({typeDefs, resolvers});
module.exports = schema;