const jwt = require('jsonwebtoken');

 const isAuthenticated  = async (req, res, next) =>{
    const authHeader = req.headers['authorization'];
    console.log("dddddddddddddddddddddd",req)
    if (authHeader && authHeader.startsWith('Bearer ')) {
      const token = authHeader.split(' ')[1];  
      try {
        const decoded = jwt.verify(token, process.env.JWTSECRET);
        req.user = decoded;
        next();
      } catch (err) {
        return res.status(401).json({
          error: 'Unauthorized',
          message: 'Invalid or expired token.',
        });
      }
    } else {
      return res.status(401).json({
        error: 'Unauthorized',
        message: 'Authorization token missing or invalid.',
      });
    }
  }

  module.exports=isAuthenticated;