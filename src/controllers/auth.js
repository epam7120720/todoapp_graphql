const users = require('../Models/db').users;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.register=async (req,res)=>{
   try{
          const { email, password } = req.body;
          const hashed = await bcrypt.hash(password,10);
          console.log(email, hashed);
         const user = await users.create({email:email , password:hashed})
            console.log(user);
            return res.status(201).json({
              message:`user ${email} registered`
            })
      }catch(err){
        console.error(err);
        return res.status(500).json({
            Error:"Server error", 
            Message:err.errors[0].message
        })
    }
  }
    exports.login = async (req,res)=>{
      try{
            const { email, password } = req.body;
            const user = await users.findOne({where:{email}});
            
             if(!user){
              return res.status(401).json({
                message:'invalid user or password .'
              })
             }
            const valid = await bcrypt.compare(password, user.password);
            if (!valid) {
              return res.status(401).json({ message: 'invalid user or password .' });
            }
            const token = jwt.sign({ user_id:user.id, email}, process.env.JWTSECRET, { expiresIn: '1h' });
            return res.status(200).json({
                user: { 
                  user_id:user.id,
                  email 
              }, 
              token
            })
        }catch(err){
          console.error(err);
          return res.status(500).json({
            error:'server error',
            message: err.errors[0].message
        });
        }
    }
        

