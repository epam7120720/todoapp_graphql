const db = require('../Models/db');
const tasks = db.tasks;

exports.addTask= async(req,res)=>{
    try{
    const {title,description}= req.body;
      if(!title || !description){
        return res.status(400).json({
          error:"Bad Request",
          message:"Schema validation of the body failed"
        })
      }

     await tasks.create({user_id: req.user.user_id,title, description});

     return res.status(200).json({
      message:`task with title : ${title} was added`
     })
    }catch(err){
        console.error(err);
        return res.status(500).json({
          error:'server error',
          message: err.errors[0].message
        });
      }
}

exports.getTasks= async(req,res)=>{
    try{
     const allTasks = await tasks.findAll({where:{user_id :req.user.user_id}})
     return res.status(200).json({ allTasks });
      
    }catch(err){
        console.error(err);
        return res.status(500).json({
        error:'server error',
        message:err.errors[0].message
      });
      }
}

exports.makeDone =async(req,res)=>{
    try{
    const{title}= req.body;
    if(!title){
      return res.status(400).json({
        error:"Bad Request",
        message:"Schema validation of the body failed"
      })
    }
    
    const task = await tasks.findOne({where:{user_id:req.user.user_id , title}})
    task.isDone=1;
   await task.save();
    if(!task){
      return res.status(404).json({
        message:'such task does not exist .'})
    }

    return res.status(200).json({
      message:`${title} is done `
    })

    }catch(err){
        console.error(err);
        return res.status(500).json({
          error:'server error',
          meeage:err.errors[0].message
        });
      }
}

exports.getDones = async(req,res)=>{
   try{
    const dones = await tasks.findAll({where:{user_id:req.user.user_id , isDone:1}})
    return res.status(200).json({
      message: dones
    })
  }catch(err){
    console.error(err);
    return res.status(500).json({
      error:'server error',
      message: err.errors[0].message
    });
  }
}

exports.editTask= async(req,res)=>{
    try{
    const newTitle = req.params.taskTitle;
      const {title: currentTitle,description}= req.body;
      
      if(!currentTitle || !description||!newTitle){
        return res.status(400).json({
          error:"Bad Request",
          message:"Schema validation of the body failed"
        })
      }
      const nTask = await tasks.findOne({where:{user_id:req.user.user_id ,title: newTitle }});
      if(nTask && currentTitle === newTitle){
        nTask.description = description;
        await tasks.save();
        return res.status(201).json({
          message:`task updated`
        })
      }
      if(nTask){
        return res.status(400).json({
          message: `task ${newTitle} already exists`
        })
      }

      const task = await tasks.findOne({where:{user_id:req.user.user_id ,title: currentTitle }});

      task.title = newTitle;
      task.description=description;
      await task.save();

      return res.status(200).json({
        message: 'task was edited succesffuly'
      })
      
    }catch(err){
        console.error(err);
        return res.status(500).json({
          error:'server error',
          mesage:err.errors[0].message
        });
    }
}

exports.deleteTask =async(req,res)=>{
    try{
    const{title}= req.body;
      if(!title){
        return res.status(400).json({
          error:"Bad Request",
          message:"Schema validation of the body failed"
        })
      }

     const task = await tasks.findOne({where:{user_id:req.user.user_id ,title}});
     if(!task){
      return res.status(200).json({message:'deleted'})
     }
    await  task.destroy();

    return res.status(200).json({message:'task deleted'})
    }catch(err){
     console.error(err);
     return res.status(500).json({
      error:'server error',
      message: err.errors[0].message
    });
      }
}